//NAME: Raphael Canciani
//ID: 2039424

public class BikeStore{
    public static void main(String args[]){
        Bicycle[] bikeArray = new Bicycle[4];
        bikeArray[0] = new Bicycle("BMW",16,51.0);
        bikeArray[1] = new Bicycle("Mercedes",21,57.0);
        bikeArray[2] = new Bicycle("McLaren",31,112.4);
        bikeArray[3] = new Bicycle("Honda",2,12.5);
        System.out.println(bikeArray[0] +"\n"+bikeArray[1] +"\n"+bikeArray[2] +"\n"+bikeArray[3] +"\n");
    }
}