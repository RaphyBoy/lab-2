//NAME: Raphael Canciani
//ID: 2039424

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String bikeMaker, int numGears, double topSpeed){
        manufacturer = bikeMaker;
        numberGears = numGears;
        maxSpeed = topSpeed;
    }
    public void getManufacturer(){

    }
    public void getNumberGears(){

    }
    public void getmaxSpeed(){

    }
    public String toString(){
        String s = "";
        s += "\n"+"Manufacturer: "+ manufacturer+"\n"+"Number of Gears: "+numberGears+"\n"+"Top speed: "+maxSpeed;
        return s;
    }
}